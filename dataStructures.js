// level 3

// 1
const user = {
    name: "Elie",
    job: "Instructor",
    workDetails: {
        type: "Part Time",
        hours: "40 hours"
    },
    educationQualifications: [
        {
            name: "Full Stack",
            type: "Certification"
        },
        {
            name: "Javascript",
            type: "Certification"
        }
    ]
}

convertToUpperCase = (data) => {
    Object.keys(data).forEach(e => {
        if (typeof data[e] !== object) {
            data[e] = data[e].toUpperCase();
        } else {
            convertToUpperCase(data[e])
        }
    })
}

convertToUpperCase(user);

console.log(user);

// ------------------------------------------------------------------------------------
// 2

str = "a:2,b:3,c:4,a:5,b:6";
obj = {};
strArr = str.split(',');
strArr.forEach(data => {
    key = data.split(':')[0];
    value = data.split(':')[1];
    if (obj[key]) {
        obj[key] = obj[key] + value;
    } else {
        obj[key] = value;
    }
})

console.log(obj);

// ---------------------------------------------------------------------------------------------
// 3

exp1 = "{[({})]}";
exp2 = "{[(]}"

library = {
    '(': ')',
    '{': '}',
    '[': ']'
}


check = (exp) => {
    stack = [];
    exp.forEach((e,i) => {
        if(library[e]) {
            stack.push(library[e]);
        } else {
            if(stack.pop() !== e) {
                return false;
            }
        }
    })
    stack.length == 0? console.log('balanced'): console.log('not balanced');
}

